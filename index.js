const {Configuration, OpenAIApi} = require('openai');
const bodyParser = require('body-parser');
const cors = require('cors');
require('dotenv').config();

const express = require('express');
const path = require('path');

const app = express();
const server = require('http').createServer(app);

// untuk mengirim data dari front end ke node.js (socket.io)
const io = require('socket.io')(server);

// koneksi ke openai nya
const config = new Configuration({
    apiKey: process.env.API_TOKEN
})

const openai = new OpenAIApi(config);

io.on('connection', function(socket){
    socket.on('newuser',function(username){
        console.log(username);
    });

    // memanggil prompt nya dari script.js
    socket.on('prompt',function(data){
        console.log(data);
        // konekin ke openai nya
        const response  = openai.createCompletion({
            model       : "text-davinci-003",
            prompt      : data.text,
            // temperatur adalah untuk semakin tinggi temperatur jawabanya akan semakin random
            temperature : 0.1,
            top_p       : 1,
            // agar tidak mengulang
            frequency_penalty : 0,
            presence_penalty : 0,
            max_tokens: 256,
            // nambahin sequent / membuat 4 step saja tanya jawab selebihnya akan ngawur jawabnya
            stop:["Human:","AI:","Human:","AI:"]
        });

        response.then((incomingData) => {
            // agar mendapatkan jawabanya saja ditambkan .text
            const message = incomingData.data.choices[0].text;
            // console.log(message);

            // berupa chat bot nya
            socket.emit('chatbot', {
                username:"bot",
                text:message
            })
        }).catch((err)=>{
            console.log(err);
        })
    });
});

app.use(express.static(path.join(__dirname+"/public")));
app.use(bodyParser.json());
app.use(cors());

server.listen(3000, () => console.log('server berjalan di localhost:3000'));
